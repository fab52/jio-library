# JIO-Library

## Introduction
A library providing a function to simplified the Java code for i/o function. The objective of this library: Minimized the line of code Provide an easy and simplified used of i/o function. Extra features for users easy to manipulate the function.

Bleeding edge build: [Jenkins build](https://jenkins-fazreil.rhcloud.com/job/PUNCH-jio_lib/)

Java Doc: (https://jenkins-fazreil.rhcloud.com/job/PUNCH-jio_lib/ws/target/site/apidocs/index.html)

Documentation: (https://jenkins-fazreil.rhcloud.com/job/PUNCH-jio_lib/ws/target/site/index.html)

## Executing JIO-Library
Here are some way to interact with JIO-Lib

### Install as maven library
Install the library using maven with
```
mvn
```

### Run springboot application
Or run the springboot application with
```
mvn spring-boot:run
```

### Running the same command also will run swagger, to build and run you may execute the following command
```
mvn clean install spring-boot:run
```
You may then browse to http://localhost:8080/swagger-ui.html 
