package org.gravity.springboot.controller;

import org.gravity.Conversion;
import org.gravity.Conversion.Length;
import org.gravity.Conversion.Weight;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/conversion")
public class ConversionController {

	
	@RequestMapping(
			value = "/api/length",
			params = {"value","from","to"},
			method = RequestMethod.GET)
	public double length(
			@RequestParam double value, 
			@RequestParam double from,
			@RequestParam double to) {
		Length len = new Conversion().new Length();
		return len.convert(value, from, to); 
	}
	
	@RequestMapping(
			value = "/api/weight",
			params = {"value","from","to"},
			method = RequestMethod.GET)
	@ResponseBody
	public double weight(
			@RequestParam("value") double value, 
			@RequestParam("from") double from, 
			@RequestParam("to") double to) {
		Weight wt = new Conversion().new Weight();
		return wt.convert(value, from, to);
	}
}
