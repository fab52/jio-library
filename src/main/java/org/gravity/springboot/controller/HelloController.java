package org.gravity.springboot.controller;

import org.springframework.web.bind.annotation.RestController;
import org.gravity.Conversion;
import org.gravity.Conversion.Length;
import org.gravity.Conversion.Weight;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class HelloController {

	@RequestMapping("/")
	public String index() {
		return "Greetings from Spring Boot!";
	}

}
