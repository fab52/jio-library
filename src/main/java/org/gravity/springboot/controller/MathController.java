package org.gravity.springboot.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/math")
public class MathController {

	@RequestMapping(
			value = "/api/plus",
			params = {"input","constant"},
			method = RequestMethod.GET)
	public int plus(
			@RequestParam int input, 
			@RequestParam int constant) {
		int ret = -1;
		try {
			ret = org.gravity.Math.plus(input, constant); 
		}
		catch(IOException io) {
			io.printStackTrace();
		}
		return ret;
	}
	
	@RequestMapping(
			value = "/api/minus",
			params = {"input","constant"},
			method = RequestMethod.GET)
	@ResponseBody
	public double minus(
			@RequestParam int input, 
			@RequestParam int constant) {
		int ret = -1;
		try {
			ret = org.gravity.Math.minus(input, constant); 
		}
		catch(IOException io) {
			io.printStackTrace();
		}
		return ret;
	}
	
	@RequestMapping(
			value = "/api/multiple",
			params = {"input","constant"},
			method = RequestMethod.GET)
	public int multiple(
			@RequestParam int input, 
			@RequestParam int constant) {
		int ret = -1;
		try {
			ret = org.gravity.Math.multiple(input, constant); 
		}
		catch(IOException io) {
			io.printStackTrace();
		}
		return ret;
	}
	
	@RequestMapping(
			value = "/api/divide",
			params = {"input","constant"},
			method = RequestMethod.GET)
	public int divide(
			@RequestParam int input, 
			@RequestParam int constant) {
		int ret = -1;
		try {
			ret = org.gravity.Math.divide(input, constant); 
		}
		catch(IOException io) {
			io.printStackTrace();
		}
		return ret;																								
	}

}
